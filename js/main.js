function turnPics() {
    var index = 0; //当前播放图片的索引
    var stop = false; //控制自动、手动的切换
    var $li = $(".banner").find("#imgs li"); //嵌套所有轮播图片的li
    var $pageIndex = $(".banner").find("#numbers li");//显示所有编号的li
    $pageIndex.eq(index).addClass("number_over")
        .stop(true, true) //清空动画序列
        .siblings()//兄弟元素
        .removeClass("number_over");
    setInterval(function () { //自动播放
        if (stop) return;
        index++;
        if (index >= $li.length) {
            index = 0;//从头播放
        }
        $li.eq(index)
            .stop(true, true)
            .fadeIn("slow")
            .siblings().fadeOut("slow");
        $pageIndex.eq(index)
            .addClass("number_over")
            .stop(true, true)
            .siblings()
            .removeClass("number_over");
    }, 3000);

    $pageIndex.mouseover(function () { //控制手动播放
        stop = true; //自动轮播结束
        index = $pageIndex.index($(this)); //当前的编号
        $li.eq(index).stop(true, true)
            .fadeIn("slow") //当前编号的图片显示
            .siblings()
            .fadeOut("slow"); //其他的图片隐藏
        $(this).addClass("number_over")
            .stop(true, true)
            .siblings()
            .removeClass("number_over");
    }).mouseout(function () {
        stop = false;
    });

}

function leftScroll() {
    var marginLeft = 0;
    var stop = false;
    setInterval(function () {
        if (stop) return;
        $(".tgjlx").find("li").first().animate({"margin-left": marginLeft--}, 0, function () {
            var $first = $(this);
            if (!$first.is(":animated")) {
                if ((-marginLeft) > $first.width() + 1 + 18) { //1：是左边框的粗细 18：左右两边的填充
                    $first.css({"margin-left": 0}).appendTo($(".tgjlx ul"));
                    marginLeft = 0;
                }
            }
        });
    }, 20);
    $(".tgjlx ul").mouseover(function () {
        stop = true;
    }).mouseout(function () {
        stop = false;
    });
}

$(document).ready(function () {
    turnPics(); //轮播
    leftScroll(); //循环向左滚动


    var isLogin = localStorage.getItem("islogin");
    console.log("islogin=" + isLogin)
    if (isLogin && isLogin == "true") {
        $('.userinfo').html("欢迎您，" + JSON.parse(localStorage.getItem("user")).name);
        $('.userinfo').show();
        $('.loginout').show();
        $('.cls').show();

        $('.btn-login').hide();
        $('.btn-regist').hide();
    } else {
        $('.userinfo').hide();
        $('.loginout').hide();
        $('.btn-login').show();
        $('.btn-regist').show();
        $('.cls').hide();
    }
    var obj = jsonData;
    $.each(obj.group, function (index, value) {
        $(".tgjlx-list").append("<li><img src=" + "img/" + value.picture + ".png" + " width=\"160\" height=\"88\"/><a href=" + "detail.html?name=" + value.pid + ">"
            + value.name + "</a><br/>" + "<span>" + "￥" + value.group_price + "</span>" + "原价￥" + value.price + "</li>");
    })


    $.each(obj.north, function (index, value) {
        $(".scenic-list").append('<div class="con-2inf"><ul><li class="w130"><a href=' + "detail.html?name=" + value.pid + '><img src=' + 'img/' + value.picture + '.png' + ' width="120" height="75"/></a>'
            + '产品编号' + value.pid + '</li><li class="w440"><div class="w440lx"><a href=' + "detail.html?name=" + value.pid + '>' + value.name + '</a></div><div class="w440xx">'
            + '简介：' + value.intro + '<br/>' + '本线路已有' + '<span>' + value.person_num + '人出游' + '</div></li><li class="w140"><br/><span>' + "￥" + value.price
            + '元/每人</span></li></ul></div>')
    })
    $("#north").bind('click', function () {
        $(".scenic-list").empty();
        $.each(obj.north, function (index, value) {
            $(".scenic-list").append('<div class="con-2inf"><ul><li class="w130"><a href=' + "detail.html?name=" + value.pid + '><img src=' + 'img/' + value.picture + '.png' + ' width="120" height="75"/></a>'
                + '产品编号' + value.pid + '</li><li class="w440"><div class="w440lx"><a href=' + "detail.html?name=" + value.pid + '>' + value.name + '</a></div><div class="w440xx">'
                + '简介：' + value.intro + '<br/>' + '本线路已有' + '<span>' + value.person_num + '人出游' + '</div></li><li class="w140"><br/><span>' + "￥" + value.price
                + '元/每人</span></li></ul></div>')
        })
    })
    $("#south").click(function () {
        $(".scenic-list").empty();
        $.each(obj.south, function (index, value) {
            $(".scenic-list").append('<div class="con-2inf"><ul><li class="w130"><a href=' + "detail.html?name=" + value.pid + '><img src=' + 'img/' + value.picture + '.png' + ' width="120" height="75"/></a>'
                + '产品编号' + value.pid + '</li><li class="w440"><div class="w440lx"><a href=' + "detail.html?name=" + value.pid + '>' + value.name + '</a></div><div class="w440xx">'
                + '简介：' + value.intro + '<br/>' + '本线路已有' + '<span>' + value.person_num + '人出游' + '</div></li><li class="w140"><br/><span>' + "￥" + value.price
                + '元/每人</span></li></ul></div>')
        })
    })
    $("#northwest").click(function () {
        $(".scenic-list").empty();
        $.each(obj.northwest, function (index, value) {
            $(".scenic-list").append('<div class="con-2inf"><ul><li class="w130"><a href=' + "detail.html?name=" + value.pid + '><img src=' + 'img/' + value.picture + '.png' + ' width="120" height="75"/></a>'
                + '产品编号' + value.pid + '</li><li class="w440"><div class="w440lx"><a href=' + "detail.html?name=" + value.pid + '>' + value.name + '</a></div><div class="w440xx">'
                + '简介：' + value.intro + '<br/>' + '本线路已有' + '<span>' + value.person_num + '人出游' + '</div></li><li class="w140"><br/><span>' + "￥" + value.price
                + '元/每人</span></li></ul></div>')
        })
    })
    $("#qinghai").click(function () {
        $(".scenic-list").empty();
        $.each(obj.qinghai, function (index, value) {
            $(".scenic-list").append('<div class="con-2inf"><ul><li class="w130"><a href=' + "detail.html?name=" + value.pid + '><img src=' + 'img/' + value.picture + '.png' + ' width="120" height="75"/></a>'
                + '产品编号' + value.pid + '</li><li class="w440"><div class="w440lx"><a href=' + "detail.html?name=" + value.pid + '>' + value.name + '</a></div><div class="w440xx">'
                + '简介：' + value.intro + '<br/>' + '本线路已有' + '<span>' + value.person_num + '人出游' + '</div></li><li class="w140"><br/><span>' + "￥" + value.price
                + '元/每人</span></li></ul></div>')
        })
    })
    $(".loginout").click(function () {
        localStorage.setItem("islogin", false);
        window.location.reload();
        alert("已成功退出登录");
    })

})


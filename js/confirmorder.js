$(function () {

    var json = localStorage.getItem("temp_order");
    console.log(json);
    var currdate = new Date();
    var date = formatDate(currdate);
    var timestamp = Date.parse(currdate);

    var temp_order = JSON.parse(json);
    $(".pay-name").text("商品名称：" + temp_order.route_name)
    $(".pay-money").text("交易金额：¥" + temp_order.route_price)
    $(".pay-time").text('购买时间：' + formatDate2(currdate))
    $(".pay-no").text('订单编号：' + date + timestamp)


    $(".confirm-pay").click(function () {
        var store = localStorage.getItem("pid");
        var pid = temp_order.pid;
        var tempobj = {
            pid: pid,
            date: temp_order.date,
        };
        if (store) {
            var arr = JSON.parse(store);
            console.log(arr);
            arr.push(tempobj);
            localStorage.setItem("pid", JSON.stringify(arr));
            location.href = "orderlist.html"
        } else {
            var arr = [tempobj];
            localStorage.setItem("pid", JSON.stringify(arr));
            location.href = "orderlist.html"
        }
    });


});

function formatDate(date) {
    var myyear = date.getFullYear();
    var mymonth = date.getMonth() + 1;
    var myweekday = date.getDate();
    var hour = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();

    if (mymonth < 10) {
        mymonth = "0" + mymonth;
    }
    if (myweekday < 10) {
        myweekday = "0" + myweekday;
    }
    return (myyear + mymonth + myweekday + hour + minutes + seconds);
}

function formatDate2(date) {
    var myyear = date.getFullYear();
    var mymonth = date.getMonth() + 1;
    var myweekday = date.getDate();
    if (mymonth < 10) {
        mymonth = "0" + mymonth;
    }
    if (myweekday < 10) {
        myweekday = "0" + myweekday;
    }
    return (myyear + "-" + mymonth + "-" + myweekday);
}

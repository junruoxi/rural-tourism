$(function () {
    $("#txtLoginNo").focus(function () {  // 输入账号的文本框获得鼠标焦点
        var txt_value = $(this).val();   // 得到当前文本框的值
        if (txt_value == "请输入6~12位账号") {
            $(this).val("");              // 如果符合条件，则清空文本框内容
        }
    });
    $("#txtLoginNo").blur(function () {// 文本框失去鼠标焦点
        var txt_value = $(this).val();   // 得到当前文本框的值
        if (txt_value == "") {
            $(this).val("请输入6~12位账号");// 如果符合条件，则设置内容
        }
    });

    $("#txtLoginPwd").focus(function () {  // 输入账号的文本框获得鼠标焦点
        var txt_value = $(this).val();   // 得到当前文本框的值
        if (txt_value == "请输入6~12位密码") {
            $(this).val("");              // 如果符合条件，则清空文本框内容
        }
    });
    $("#txtLoginPwd").blur(function () {// 文本框失去鼠标焦点
        var txt_value = $(this).val();   // 得到当前文本框的值
        if (txt_value == "") {
            $(this).val("请输入6~12位密码");// 如果符合条件，则设置内容
        }
    });
});


function checkLogin() {
    var valid = checkLoginNo($("#txtLoginNo"))
        && checLoginkPwd($("#txtLoginPwd"));
    console.log("txtLoginNo="+$("#txtLoginNo").val() + "     txtPwd=" + $("#txtLoginPwd").val())

    var json = localStorage.getItem("user");
    console.log(json)
    if (!json) {
        alert("请先注册账号")
        return false;
    }
    var user = JSON.parse(json);
    console.log("valid=" + valid)
    console.log("user.username =" + user.username )
    console.log("user.password =" + user.password )
    if (valid) {
        console.log(user.username == ($("#txtLoginNo").val()));
        console.log(user.password == ($("#txtLoginPwd").val));
        if (user.username == ($("#txtLoginNo").val())
            /*& user.password ==($("#txtLoginPwd").val)*/) {
            localStorage.setItem("islogin","true");
            return true;
        } else {
            alert("用户名和密码错误")
        }
    }
    return false;
}

function checkLoginNo($txtObj) {//账号必填，且长度在6到12之间
    var len = $txtObj.val().length;
    if (len == 0) {
        $txtObj.siblings("span").text("账号必填");
        return false;
    }
    if (len < 6 || len > 12) {
        $txtObj.siblings("span").text("账号的长度在6到12位之间");
        return false;
    }
    return true;
}

function checLoginkPwd($txtObj) {
    var len = $txtObj.val().length;
    console.log(len)
    if (len == 0) {
        $txtObj.siblings("span").text("密码必填");
        return false;
    }
    if (len < 6 || len > 12) {
        $txtObj.siblings("span").text("密码的长度在6到12位之间");
        return false;
    }
    return true;
}
$(document).ready(function () {

    $(".head").load("header.html");
    $("#foot").load("footer.html");

    var index = 0; //当前播放图片的索引 窗口加载时，播放第一幅图片
    var stop = false; //控制自动、手动的切换
    var $li = $("ul.jnImgs").find("li"); //组织所有轮播图片的li
    var $pageIndex = $("ul.jnText").find("li");//组织所有文本的li
    $pageIndex.eq(index).addClass("current")
        .stop(true, true) //清空动画序列
        .siblings()//兄弟元素
        .removeClass("current");
    $pageIndex.mouseover(function () { //控制手动播放
        stop = true; //自动轮播结束
        index = $pageIndex.index($(this)); //当前的编号
        $li.eq(index).stop(true, true)
            .fadeIn() //当前编号的图片显示
            .siblings()
            .fadeOut(); //其他的图片隐藏
        $(this).addClass("current")
            .stop(true, true)
            .siblings()
            .removeClass("current");
    }).mouseout(function () {
        stop = false;
    });
    setInterval(function () { //自动播放
        if (stop) return;
        index++;
        if (index >= $li.length) {
            index = 0;//从头播放
        }
        $li.eq(index)
            .stop(true, true)
            .fadeIn()
            .siblings().fadeOut();
        $pageIndex.eq(index)
            .addClass("current")
            .stop(true, true)
            .siblings()
            .removeClass("current");
    }, 3000);

    var obj = jsonData;
    $.each(obj.activities, function (index, value) {
        console.log(value.name);

        $('.choiceness-activities-list').append(
            "<li><a href=" + "detail.html?name=" + value.pid + "><img src=" + "img/" + value.picture + ".png" + " width=\"180\" height=\"120\"/></a>时间：2021.5.8-7.5<p>" + value.name + "</p></li>"
        )
    });

    $.each(obj.group, function (index, group_value) {
        if (index < 3) {
            $('.choiceness-group-list').append(
                "<li>" +
                "<div class=\"w16\"><img src=" + "img/" + group_value.picture + ".png" + " width=\"160\" height=\"120\"/></div>" +
                "<div class=\"w16-r\">(团购)" + group_value.name + "</div>" +
                "<div class=\"w16-rx\">团购价<span>￥" + group_value.price + "</span>节省￥" + (group_value.price - group_value.group_price) + "<a href=" + "detail.html?name=" + group_value.pid + "><img src=\"img/qg.jpg\" width=\"92\" height=\"32\"/></a></div>" +
                "</li>"
            )
        }
    });

    $.each(obj.recommend, function (index, recommend_value) {
        $('.choiceness-recommend-list').append(
            "<li><a href=" + "detail.html?name=" + recommend_value.pid + "><img src=" + "img/" + recommend_value.picture + ".png" + " width=\"240\" height=\"160\"/></a><span>推荐理由：</span>" + recommend_value.intro + "</li>"
        )
    })

});
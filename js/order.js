$(function () {

    var url = decodeURI(window.location.href);
    var index = url.indexOf('=');
    var buy_num = 1;
    if (index > -1) {
        var pid = url.substring(index + 1);
        var obj = jsonData;
        $.each(obj.all_scenice, function (index, value) {
            console.log(pid)
            if (pid == value.pid) {
                $(".route-no").text(value.pid)
                $(".route-name").text(value.name)
                $(".route-price").text("¥" + (value.is_group ? value.group_price : value.price))
            }
        })
    }

    var setting = {
        changeMonth: true, //显示选择月份的下拉列表
        changeYear: true,//显示选择年份的下拉列表
        showWeek: true,//显示日期对应的星期
        showButtonPanel: true,//显示"关闭"按钮面板
        closeText: "关闭",//设置关闭按钮的文本
        yearRange: '2021:2030',//设置年份的范围
        dateFormat: 'yy-mm-dd',//设置显示在文本框中的日期格式
        showAnim: "slideDown"//设置显示或隐藏日期选择窗口的方式。可以设置的方式有："show"、"slideDown"、"fadeIn"
    };
    $("#txtLeaveDate").datepicker(setting); //文本框和日历插件绑定

    //判断选择的出行日期必须今天
    $("#txtLeaveDate").change(function () {
        var now = new Date();  //今天的时间
        var future = new Date($(this).val());
        var diff = future.getTime() - now.getTime();//毫秒
        var days = diff / (1000 * 60 * 60 * 24); //相差的天数
        if (days <= 0) {
            $(this).next("span").text("出行的日期必须大于今天");
            $(this).val("");
        } else {
            $(this).next("span").text("");
        }
    });


    //删除该行的信息录入表,删除之前备份
    var copyInfo = $(".vistor_info").html(); // 避免把所有的信息表都删除后，没复制的html原件
    $("a.operateDel").click(function () {
        var flag2 = confirm("确认删除该出游人吗？");
        if (flag2) {
            $(this).parents("div.dingdan-mm").remove();
            buy_num--;
        }
    });
    //增加出游人的信息录入表
    $(".people a.operateAdd").click(function () {
        var $addInfo = $(copyInfo);
        $(this).parent("div").before($addInfo);
        //找增加的信息表中的”删除“a标签
        buy_num++;
        $addInfo.find("a.operateDel").bind("click", function () {

            var flag = confirm("确认删除该出游人吗？");
            if (flag) {
                $(this).parents("div.dingdan-mm").remove();
                buy_num--;
            }
            return false;
        });//给新增加的"删除"链接绑定方法
        return false;
    });

    $(".next-step").click(function () {
        var url = decodeURI(window.location.href);
        var index = url.indexOf('=');
        console.log(index)
        console.log(buy_num)
        if (index > -1) {
            var pid = url.substring(index + 1);
            var realName = $("#realname").val();
            var phone = $("#phone").val();
            var card = $("#card").val();
            var cyri = $("#txtLeaveDate").val();

            if (cyri.length == 0) {
                alert("出游日期必选")
                return;
            }

            if (realName.length == 0) {
                alert("真实姓名必填")
                return;
            }

            var phoneReg = /^(13|15|18)\d{9}$/;
            if (phone.length == 0) {
                alert("手机号必填")
                return;
            }
            if (!phoneReg.test(phone)) {
                alert("手机格式错误")
                return;
            }
            var idReg = /^\d{15}$|^\d{18}$/;
            if (card.length == 0) {
                alert("身份证必填")
                return;
            }
            if (!idReg.test(card)) {
                alert("身份证格式错误");
                return;
            }
            $.each(obj.all_scenice, function (index, value) {
                console.log(pid)
                if (pid == value.pid) {
                    var temp_order = {
                        route_no: value.pid,
                        route_name: value.name,
                        route_price: value.is_group ? value.group_price : value.price,
                        pid: pid,
                        date: $("#txtLeaveDate").val()
                    };
                    console.log(JSON.stringify(temp_order))
                    localStorage.setItem("temp_order", JSON.stringify(temp_order));
                    location.href = "confirmorder.html"
                }
            });
        }
    })
});

//删除的功能
function del($dela) { //<a>标签对应的div
    var flag = confirm("确认删除该出游人吗？");
    if (flag) {
        $dela.parents("div.dingdan-mm").remove();
    }
}

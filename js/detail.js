$(function () {

    $("#buy").click(function () {
        var url = decodeURI(window.location.href);
        var index = url.indexOf('=');
        if (index > -1) {
            var pid = url.substring(index + 1);
            location.href="order.html?"+"pid="+pid;
        }
    })

    $(".zoom").jqzoom()
    var url = decodeURI(window.location.href);
    var index = url.indexOf('=');
    if (index > -1) {
        var pid = url.substring(index + 1);

        var obj = jsonData;
        $.each(obj.all_scenice, function (index, value) {
            if (pid == value.pid) {
                $(".jindck-meun").html(value.name);
                //图片
                $(".detail-images").append(
                    "<li>" +
                    "<a href=\"\" title=\"\">" +
                    "<img src=" + "img/" + value.picture + ".png" + " width=\"400\" height=\"250\"/>" +
                    "</a>" +
                    "</li>"
                )
                for (var i = 1; i < 4; i++) {
                    if (i == 1) {
                        $(".detail-images").append(
                            "<li class=\"pics\">" +
                            "<a href=" + "img/" + value.picture + ".png" + " class=\"zoom\" title=" + value.name + ">" +
                            "<img src=" + "img/" + value.picture + ".png" + " width=\"100\" height=\"63\"/>" +
                            "</a>" +
                            "</li>"
                        )
                    } else {
                        $(".detail-images").append(
                            "<li class=\"pics\">" +
                            "<a href=" + "img/" + value.picture + i + ".png" + " class=\"zoom\" title=" + value.name + ">" +
                            "<img src=" + "img/" + value.picture + i + ".png" + " width=\"100\" height=\"63\"/>" +
                            "</a>" +
                            "</li>"
                        )
                    }
                }

                $(".price").text("￥" + value.price);
                if (value.is_group) {
                    $("#group-price").show();
                    $("#group-price").text("￥" + value.group_price);

                } else {
                    $("#group-price").hide();
                }
                $("#discounts-price").text("最大优惠：￥" + (value.price - value.group_price));
                $("#persion-num").text(value.person_num);

                $(".jianjie-content").html(value.detail);
            }
        })
    }
});
